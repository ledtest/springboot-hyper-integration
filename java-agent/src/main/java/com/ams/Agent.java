package com.ams;
 
import java.lang.instrument.Instrumentation;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
public class Agent {
	public static void premain(String agentArgs, Instrumentation inst) {
		inst.addTransformer(new MyTransformer(agentArgs));
	}
 
	public static void premain(String agentArgs) {
	}
}