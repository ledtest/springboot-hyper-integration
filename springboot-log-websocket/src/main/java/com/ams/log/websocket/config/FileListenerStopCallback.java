package com.ams.log.websocket.config;
/**
 * @author： 乐哥聊编程(全平台同号)
 */

/**
 * 文件监听停止事件回调
 */
public interface FileListenerStopCallback {
    /**
     * 处理查询出来的数据
     * @param
     */
    boolean boolStop();
}
