package com.ams.springbootmapstruct.dto;

import lombok.Data;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
public class StudentDto {
    private String userName;
    private String userId;
    private String address;
    private String school;
    private int age;
    private String email;
    private List<CourseDto> courseDtos;
}
