package com.ams.springbootmapstruct.vo;

import com.ams.springbootmapstruct.dto.CourseDto;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
@Builder
public class StudentVo {
    private String userName;
    private String userId;
    private String address;
    private String school;
    private int age;
    private List<CourseDto> courseDtos;
    private String emailAddress;
}
