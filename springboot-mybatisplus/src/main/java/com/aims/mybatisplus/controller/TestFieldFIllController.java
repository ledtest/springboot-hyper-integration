package com.aims.mybatisplus.controller;

import com.aims.mybatisplus.dao.MemberMapper;
import com.aims.mybatisplus.model.entity.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/**
 * @author： 乐哥聊编程(全平台同号)
 */
@RestController
@RequestMapping("/testFieldFill")
public class TestFieldFIllController {
    @Autowired
    private MemberMapper memberMapper;
    @RequestMapping("insertFill")
    public String insertFill(){
        Member member = new Member();
        member.setMemberName("测试字段填充插入");
        memberMapper.insert(member);
        return "success";
    }
    @RequestMapping("updateFill")
    public String updateFill(@RequestParam long id){
        Member member = new Member();
        member.setMemberName("更新填充");
        member.setId(id);
        memberMapper.updateById(member);
        return "success";
    }
}
