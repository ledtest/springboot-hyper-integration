package com.aims.mybatisplus.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Data
@TableName("member")
public class Member implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * 主键
    */
    private Long id;

    /**
    * 创建人
    */
    private String createBy;

    /**
    * 创建时间
    */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /**
    * 更新人
    */
    private String updateBy;

    /**
    * 更新时间
    */
    @TableField(fill = FieldFill.UPDATE)
    private Date updateTime;

    /**
    * 会员名称
    */
    private String memberName;

    /**
    * 会员类型
    */
    private int memberType;

    /**
    * 手机号
    */
    private String memberPhone;

    /**
    * 会员等级
    */
    private int memberLevel;
    /**
    * 会员等级
    */
    private String tenantId;


    public Member() {}
}