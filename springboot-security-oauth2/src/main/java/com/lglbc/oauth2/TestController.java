package com.lglbc.oauth2;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author： 乐哥聊编程(全平台同号)
 * @date: 2022/5/28
 */
@RestController
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/read")
    public String read(){
        System.out.println("read");
        return "read";
    }
    @RequestMapping("/write")
    public String write(){
        System.out.println("write");
        return "write";
    }
}
