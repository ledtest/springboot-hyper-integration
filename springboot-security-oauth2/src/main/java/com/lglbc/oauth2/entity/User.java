package com.lglbc.oauth2.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @description 用户信息表
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-27
 */
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    /**
    * id
    */
    private Long id;

    /**
    * 用户名
    */
    private String username;

    /**
    * 昵称
    */
    private String nickname;

    /**
    * 性别：1-男 2-女
    */
    private int gender;

    /**
    * 密码
    */
    private String password;

    /**
    * 用户头像
    */
    private String avatar;

    /**
    * 联系方式
    */
    private String mobile;

    /**
    * 用户状态：1-正常 0-禁用
    */
    private int status;

    /**
    * 用户邮箱
    */
    private String email;

    /**
    * 逻辑删除标识：0-未删除；1-已删除
    */
    private int deleted;

    /**
    * 创建时间
    */
    private Date createTime;

    /**
    * 更新时间
    */
    private Date updateTime;

    /**
    * 更新者
    */
    private String updateBy;

    /**
    * 创建者
    */
    private String createBy;

    public User() {}
}