package com.lglbc.oauth2.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lglbc.oauth2.entity.UserRole;
import com.lglbc.oauth2.mapper.UserRoleMapper;
import com.lglbc.oauth2.service.UserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Service
@RequiredArgsConstructor
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {
}
