package com.lglbc.oauth2.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lglbc.oauth2.entity.Role;
import org.springframework.stereotype.Service;

/**
 * @description 角色表服务层
 * @author zhengkai.blog.csdn.net
 * @date 2022-05-27
 */
@Service
public interface RoleService extends IService<Role> {



}