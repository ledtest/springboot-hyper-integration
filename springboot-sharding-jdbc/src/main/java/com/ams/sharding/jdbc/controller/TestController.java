package com.ams.sharding.jdbc.controller;

import com.ams.sharding.jdbc.domain.Order;
import com.ams.sharding.jdbc.service.OrderService;
import com.ams.sharding.jdbc.util.SnowflakeIdWorker;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {
private final OrderService orderService;
    @RequestMapping("/sharding")
    public String sharding() {
        Order order = Order.builder()
                .count(10)
                .money(10L)
                .productId(10L)
                .status(1)
                .userId(1L)
                .build();
            Long activityId = SnowflakeIdWorker.generateId();
        order.setActivityId(activityId);
        orderService.save(order);
        long db = activityId%3;
        long table = activityId%3;
        return String.format("数据落在 %s 库 %s 表 分片ID:%s", db,table,activityId);
    }

}
