package com.ams.sharding.jdbc.mapper;

import com.ams.sharding.jdbc.domain.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * @author： 乐哥聊编程(全平台同号)
 */
@Repository
public interface OrderMapper extends BaseMapper<Order> {

}




